import { useEffect, useState } from 'react';

import { MIN_CHARS_SEARCH } from 'utils/constants';
import { getTop } from 'utils/helpers';
import { useRootContext } from 'context/RootContext';

const useToggleList = (resultList: any, loadingList: boolean, resultSearched: any, loadingSearched: boolean): any[] => {
  const { query } = useRootContext();
  const [list, setList] = useState<any[]>([]);

  useEffect(() => {
    if (!loadingList && resultList && resultList.results && query?.length < MIN_CHARS_SEARCH) {
      setList(getTop(resultList.results));
    }
  }, [loadingList, resultList, query]);

  useEffect(() => {
    if (!loadingSearched && resultSearched && resultSearched.results && query?.length >= MIN_CHARS_SEARCH) {
      setList(getTop(resultSearched.results));
    }
  }, [loadingSearched, resultSearched, query]);

  return list;
};

export default useToggleList;
