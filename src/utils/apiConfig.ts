import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
});

export const setupApiConfig = () => {
  axiosInstance.defaults.headers.common['Authorization'] = `Bearer ${process.env.REACT_APP_READ_ACCESS_TOKEN}`;
  axiosInstance.defaults.headers.common['Content-Type'] = 'application/json;charset=utf-8';
};

export default axiosInstance;
