import { lazy } from 'react';
import { matchPath } from 'react-router-dom';

const Movies = lazy(() => import('components/Movies/Movies'));
const MovieDetails = lazy(() => import('components/MovieDetails/MovieDetails'));
const Shows = lazy(() => import('components/Shows/Shows'));
const ShowDetails = lazy(() => import('components/ShowDetails/ShowDetails'));

const routes = {
  HOME: {
    id: 'HOME',
    path: '/',
    exact: true,
    Component: Shows,
    backEnabled: false,
  },
  MOVIES: {
    id: 'MOVIES',
    path: '/movies',
    exact: true,
    Component: Movies,
    backEnabled: false,
  },
  MOVIE_DETAILS: {
    id: 'MOVIE_DETAILS',
    path: '/movies/:id',
    exact: true,
    Component: MovieDetails,
    backEnabled: true,
  },
  SHOWS: {
    id: 'SHOWS',
    path: '/shows',
    exact: true,
    Component: Shows,
    backEnabled: false,
  },
  SHOW_DETAILS: {
    id: 'SHOW_DETAILS',
    path: '/shows/:id',
    exact: true,
    Component: ShowDetails,
    backEnabled: true,
  },
};

/**
 * Find a matching route in all application routes
 * @param pathname     Path to search for in routes.
 * @param exact       Find exact match or first partial match.
 */
export function matchRoute(pathname: string, exact: boolean): any {
  return Object.values(routes).find(r =>
    matchPath(pathname, {
      path: r.path,
      exact: exact == null ? r.exact : exact, // if exact flag provided use it otherwise use as specified in route r
    })
  );
}

export default routes;
