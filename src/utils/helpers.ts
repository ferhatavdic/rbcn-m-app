import { IMG_DEFAULT_WIDTH, IMG_URL_PREFIX } from 'utils/constants';

export const getTop = (list: any[], limit = 10) => {
  const topList: any[] = [];
  if (list && list.length > 0) list.forEach((x, i) => i < limit && topList.push(x));
  return topList;
};

export const generateImgUrl = (path: string, width = IMG_DEFAULT_WIDTH) => IMG_URL_PREFIX + width + path;
