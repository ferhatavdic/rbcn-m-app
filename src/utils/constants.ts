export const HTTP_VERBS = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE',
  PATCH: 'PATCH',
};

export const API_PATHS = {
  MOVIES_TOP: 'movie/top_rated',
  SHOWS_TOP: 'tv/top_rated',
  MOVIES_SEARCH: (query: string) => `search/movie?query=${query}`,
  SHOWS_SEARCH: (query: string) => `search/tv?query=${query}`,
  MOVIE_IMAGES: (id: number) => `movie/${id}/images`,
  SHOW_VIDEOS: (id: number) => `tv/${id}/videos`,
  SHOW_DETAILS: (id: number) => `tv/${id}?append_to_response=videos`,
  MOVIE_DETAILS: (id: number) => `movie/${id}?append_to_response=images`,
};

export const DEBOUNCE_TIMEOUT = 1000;
export const MIN_CHARS_SEARCH = 3;

export const IMG_DEFAULT_WIDTH = 500;
export const IMG_URL_PREFIX = 'https://image.tmdb.org/t/p/w';

export const BREAKPOINTS = {
  SM: 575,
  MD: 768,
  LG: 991,
  XL: 1200,
};
