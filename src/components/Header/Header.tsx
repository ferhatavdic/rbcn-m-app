import React from 'react';
import { Container, Row, Col, Navigation, Link } from 'components/Root/Root.styles';

import routes, { matchRoute } from 'utils/routes';
import { useRootContext } from 'context/RootContext';
import { BackButton } from 'components';

const Header = () => {
  const { currentPath } = useRootContext();
  return (
    <Container>
      <header>
        <Row>
          <Col className="px-5 d-flex justify-space-between mb-5">
            <Navigation>
              <Link to="/" active={currentPath.includes(routes.SHOWS.path) || currentPath === routes.HOME.path}>
                Shows
              </Link>
              <Link to="/movies" active={currentPath.includes(routes.MOVIES.path)}>
                Movies
              </Link>
            </Navigation>
            {matchRoute(currentPath, true).backEnabled && (
              <div className="d-flex align-items-center">
                <BackButton />
              </div>
            )}
          </Col>
        </Row>
      </header>
    </Container>
  );
};

export default Header;
