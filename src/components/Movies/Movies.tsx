import React from 'react';
import { API_PATHS } from 'utils/constants';
import useApi from 'utils/hooks/useApi';
import useToggleList from 'utils/hooks/useToggleList';
import { useRootContext } from 'context/RootContext';
import routes from 'utils/routes';
import { ListLayout } from 'components';

const Movies = () => {
  const { resultSearchMovies, loadingSearchMovies } = useRootContext();
  const [{ result: resultMovies, loading: loadingMovies }] = useApi(API_PATHS.MOVIES_TOP);
  const movies = useToggleList(resultMovies, loadingMovies, resultSearchMovies, loadingSearchMovies);

  return (
    <ListLayout
      displayedList={movies}
      loadingList={loadingMovies}
      loadingSearchedList={loadingSearchMovies}
      detailsPath={routes.MOVIES.path}
    />
  );
};

export default Movies;
