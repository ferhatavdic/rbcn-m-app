import React from 'react';
import { Genres } from 'components';
import { Rating, Card, CardBody } from './ItemDetails.styles';
import { Row, Col } from 'components/Root/Root.styles';

type Props = {
  title?: string;
  rating?: number;
  synopsys?: string;
  genres?: any[];
  CardHeader?: React.FunctionComponent;
  children?: React.ReactNode;
};
const ItemDetails = ({ title, rating, genres, synopsys, CardHeader, children }: Props) => (
  <Row>
    <Col className="px-5">
      <Card className="mb-5">
        <CardHeader />
        <CardBody>
          <div className="d-flex justify-space-between align-items-center flex-wrap">
            <h2 className="mt-0">{title}</h2>
            <Rating>{rating}</Rating>
          </div>
          <div className="mb-5">
            <Genres list={genres} />
          </div>
          <div className="text-justify">{synopsys}</div>
          {children}
        </CardBody>
      </Card>
    </Col>
  </Row>
);

export default ItemDetails;
