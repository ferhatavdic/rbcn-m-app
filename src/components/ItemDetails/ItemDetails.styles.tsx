import styled from 'styled-components';

export const Card = styled.div`
  background-color: var(--secondary);
  box-shadow: 1px 1px 25px 0px rgba(0, 0, 0, 0.25);
  border-radius: 4px;
`;

export const CardBody = styled.div`
  padding: 1.5rem;
`;

export const Rating = styled.div`
  border-radius: 50%;
  width: 50px;
  height: 50px;
  justify-content: center;
  background-color: var(--primary);
  color: var(--light);
  display: flex;
  align-items: center;
  margin-bottom: 1rem;
`;
