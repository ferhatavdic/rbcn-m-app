import React from 'react';
import { generateImgUrl } from 'utils/helpers';
import { useHistory } from 'react-router-dom';
import { CardContainer, CardImg, CardImgPlaceholder, CardTitle, Score, CardImgWrapper } from './CardItem.styles';

type Item = {
  id?: number;
  title?: string;
  name?: string;
  vote_average?: number;
  release_date?: string;
  first_air_date?: string;
  overview?: string;
  poster_path?: string;
};

type Props = {
  item: Item;
  detailsPath: string;
};

const posterWidth: number = 400;

const CardItem = ({ item = {}, detailsPath }: Props) => {
  const history: any = useHistory();
  const { id, title, name, vote_average, poster_path }: Item = item;
  const itemTitle: string = title ? title : name;

  const toDetails = (): void => {
    history.push(`${detailsPath}/${id}`);
  };
  return (
    <CardContainer width={posterWidth}>
      <CardImgWrapper>
        {poster_path ? (
          <CardImg src={generateImgUrl(poster_path, posterWidth)} alt={itemTitle} onClick={toDetails} />
        ) : (
          <CardImgPlaceholder onClick={toDetails}>No Photo</CardImgPlaceholder>
        )}
        <Score>{vote_average}</Score>
      </CardImgWrapper>
      <CardTitle>{itemTitle}</CardTitle>
    </CardContainer>
  );
};

export default CardItem;
