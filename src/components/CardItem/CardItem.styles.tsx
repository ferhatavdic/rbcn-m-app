import styled from 'styled-components';

export const CardImg = styled.img`
  cursor: pointer;
  box-shadow: 1px 1px 25px 0px rgba(0, 0, 0, 0.25);
  transition: all 300ms;
  &:hover {
    box-shadow: 1px 1px 25px 2px rgba(0, 0, 0, 0.5);
    transition: all 300ms;
  }
  object-fit: cover;
  object-position: center;
  width: 100%;
  max-height: 600px;
  border-radius: 4px;
`;

export const CardImgPlaceholder = styled.div`
  background-color: #000;
  height: 600px;
  width: 100%;
  position: relative;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const CardImgWrapper = styled.div`
  position: relative;
  max-height: 600px;
  width: 100%;
`;

type CardContainerProps = {
  width?: number;
};

export const CardContainer = styled.div<CardContainerProps>`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  margin-bottom: 1.5rem;
`;

export const CardTitle = styled.h6`
  margin-top: 0.5rem;
  align-self: flex-start;
`;

export const Score = styled.div`
  position: absolute;
  top: -1rem;
  left: calc(50% - 3rem);
  border-radius: 4px;
  background-color: var(--secondary);
  width: 6rem;
  height: 2rem;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 1rem;
  box-shadow: 1px 1px 25px 0px rgba(0, 0, 0, 0.25);
`;
