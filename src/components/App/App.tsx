import React from 'react';
import { Root } from 'components';
import { BrowserRouter } from 'react-router-dom';
import { RootProvider } from 'context/RootContext';
import { setupApiConfig } from 'utils/apiConfig';
import GlobalStyle from 'globalStyle';

function App() {
  setupApiConfig();

  return (
    <>
      <GlobalStyle />
      <BrowserRouter>
        <RootProvider>
          <Root />
        </RootProvider>
      </BrowserRouter>
    </>
  );
}

export default App;
