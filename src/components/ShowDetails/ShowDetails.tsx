import React from 'react';
import { useParams } from 'react-router-dom';
import { API_PATHS } from 'utils/constants';
import useApi from 'utils/hooks/useApi';
import { Loader, ItemDetails } from 'components';
import { BackdropImgPlaceholder } from 'components/MovieDetails/MovieDetails.styles';

type ShowDetailsT = {
  genres: any[];
  overview: string;
  poster_path: string;
  name: string;
  vote_average: number;
  videos: any;
  number_of_episodes: number;
  number_of_seasons: number;
  last_air_date: string;
  status: string;
};

const ShowDetails = () => {
  const { id } = useParams();
  const [{ result: resultShowDetails, loading: loadingShowDetails }] = useApi(API_PATHS.SHOW_DETAILS(id));
  const {
    genres,
    overview,
    name,
    vote_average,
    videos,
    number_of_episodes,
    number_of_seasons,
    last_air_date,
    status,
  }: ShowDetailsT = resultShowDetails ? resultShowDetails : {};

  return !loadingShowDetails && resultShowDetails ? (
    <ItemDetails
      CardHeader={() =>
        videos && videos.results && videos.results[0] ? (
          <div className="video-wrapper">
            <iframe
              title={name}
              width="100%"
              height="400"
              src={`https://www.youtube.com/embed/${videos?.results[0]?.key}`}
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            ></iframe>
          </div>
        ) : (
          <BackdropImgPlaceholder>No Video</BackdropImgPlaceholder>
        )
      }
      title={name}
      rating={vote_average}
      genres={genres}
      synopsys={overview}
    >
      <div className="mt-3 mb-3">Last Air Date: {last_air_date}</div>
      <div className="mb-3">Status: {status}</div>
      <div className="mb-3">Episodes: {number_of_episodes}</div>
      <div className="mb-3">Seasons: {number_of_seasons}</div>
    </ItemDetails>
  ) : (
    <Loader />
  );
};

export default ShowDetails;
