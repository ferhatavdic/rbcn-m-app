import styled from 'styled-components';

export const BackdropImg = styled.img`
  object-fit: cover;
  object-position: center;
  width: 100%;
  max-height: 400px;
`;

export const BackdropImgPlaceholder = styled.div`
  width: 100%;
  height: 400px;
  background-color: #000;
  display: flex;
  align-items: center;
  justify-content: center;
`;
