import React from 'react';
import { useParams } from 'react-router-dom';
import { API_PATHS } from 'utils/constants';
import useApi from 'utils/hooks/useApi';
import { generateImgUrl } from 'utils/helpers';
import { BackdropImg, BackdropImgPlaceholder } from './MovieDetails.styles';
import { Loader, ItemDetails } from 'components';

type MovieDetailsT = {
  genres?: any[];
  overview: string;
  poster_path: string;
  title: string;
  vote_average: number;
  images: any;
  release_date: string;
};

const MovieDetails = () => {
  const { id } = useParams();
  const [{ result: resultMovieDetails, loading: loadingMovieDetails }] = useApi(API_PATHS.MOVIE_DETAILS(id));
  const { genres, overview, title, vote_average, images, release_date }: MovieDetailsT = resultMovieDetails
    ? resultMovieDetails
    : {};

  return !loadingMovieDetails && resultMovieDetails ? (
    <ItemDetails
      title={title}
      rating={vote_average}
      genres={genres}
      synopsys={overview}
      CardHeader={() =>
        images.backdrops && images.backdrops[0] ? (
          <BackdropImg src={generateImgUrl(images.backdrops[0]?.file_path, 500)} alt={title} />
        ) : (
          <BackdropImgPlaceholder>No Photo</BackdropImgPlaceholder>
        )
      }
    >
      <div className="mt-3 mb-3">Release Date: {release_date}</div>
    </ItemDetails>
  ) : (
    <Loader />
  );
};

export default MovieDetails;
