import React from 'react';
import { Genre } from './Genres.styles';

type Props = {
  list: any[];
};
const Genres = ({ list }: Props) => (
  <div className="d-flex flex-wrap">
    {list?.map((x: any, i, a) => (
      <Genre key={x.id} className={i + 1 < a.length && 'mr-2'}>
        {x.name}
      </Genre>
    ))}
  </div>
);

export default Genres;
