import styled from 'styled-components';
export const Genre = styled.div`
  padding: 0.25rem 1rem;
  background-color: var(--light);
  color: var(--background);
  border-radius: 4px;
  margin-bottom: 0.5rem;
`;
