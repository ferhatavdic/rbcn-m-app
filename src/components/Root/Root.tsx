import React, { Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';
import routes from 'utils/routes';
import { Container } from './Root.styles';
import { Header, Loader } from 'components';

function Root() {
  return (
    <>
      <Header />
      <main>
        <Switch>
          <Suspense fallback={<Loader />}>
            {Object.values(routes).map((r, i) => (
              <Container>
                <Route key={i} path={r.path} exact={r.exact} component={r.Component} />
              </Container>
            ))}
          </Suspense>
        </Switch>
      </main>
    </>
  );
}

export default Root;
