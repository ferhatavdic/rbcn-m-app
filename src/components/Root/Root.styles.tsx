import styled, { css } from 'styled-components';
import { Link as DefaultLink } from 'react-router-dom';
import { BREAKPOINTS } from 'utils/constants';

export const Container = styled.div`
  max-width: 960px;
  width: 100%;
  padding: 0 1rem;
  margin: 0 auto;
`;

export const Row = styled.div`
  display: flex;
`;

export const Col = styled.div`
  position: relative;
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;
`;

export const Navigation = styled.div`
  margin: 1rem 0;
  & > a:first-child {
    border-top-left-radius: 4px;
    border-bottom-left-radius: 4px;
    border-right: 0;
  }
  & > a:last-child {
    border-left: 0;
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
  }
`;

export const Link = styled(DefaultLink)<any>`
  font-size: 1rem;
  display: inline-block;
  padding: 0.5rem 1rem;
  color: var(--light);
  text-align: center;
  &,
  &:visited {
    ${({ active }) => {
      return active
        ? css`
            background-color: var(--primary);
          `
        : css`
            background-color: var(--light);
            color: var(--background);
          `;
    }}
  }
  @media only screen and (min-width: ${BREAKPOINTS.SM}px) {
    & {
      min-width: 148px;
    }
  }
`;
