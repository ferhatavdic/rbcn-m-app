import styled from 'styled-components';
import { BREAKPOINTS } from 'utils/constants';

const Button = styled.button`
  padding: 0.5rem 1rem;
  margin: 0.5rem 0;
  border-radius: 4px;
  background-color: var(--light);

  color: var(--background);
  border: 1px solid var(--light);
  @media only screen and (min-width: ${BREAKPOINTS.SM}px) {
    & {
      min-width: 148px;
    }
  }
`;

export default Button;
