import React from 'react';
import { useHistory } from 'react-router-dom';
import Button from 'components/Button/Button';

const BackButton = () => {
  const history: any = useHistory();
  return <Button onClick={() => history.goBack()}>Back</Button>;
};

export default BackButton;
