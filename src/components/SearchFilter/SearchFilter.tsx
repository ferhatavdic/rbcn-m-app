import React, { useState, useEffect } from 'react';
import { DEBOUNCE_TIMEOUT } from 'utils/constants';
import useDebounce from 'utils/hooks/useDebounce';
import Input from 'components/Input/Input';

type Props = {
  searchTerm?: string;
  placeholder?: string;
  onSearch?: (value?: string) => void;
  [x: string]: any;
};

const FilterInput = ({ searchTerm = '', placeholder = '', onSearch = () => {}, ...rest }: Props) => {
  const [keyword, setKeyword] = useState<string>(searchTerm);
  const debouncedSearchTerm: string = useDebounce(keyword, DEBOUNCE_TIMEOUT);

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const search = e.target.value;
    setKeyword(search);
  };

  useEffect(() => {
    onSearch(debouncedSearchTerm);
  }, [debouncedSearchTerm, onSearch]);

  return <Input className="mt-0" placeholder={placeholder} onChange={onChange} value={keyword} {...rest} />;
};

export default FilterInput;
