import React from 'react';
import { Row, Col } from 'components/Root/Root.styles';

const Loader = () => {
  return (
    <Row className="h-100">
      <Col className="d-flex align-items-center justify-content-center">Loading</Col>
    </Row>
  );
};

export default Loader;
