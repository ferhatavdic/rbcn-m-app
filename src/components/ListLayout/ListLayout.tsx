import React from 'react';
import { SearchFilter, CardItem } from 'components';
import { useRootContext } from 'context/RootContext';
import { Row, Col } from 'components/Root/Root.styles';
import { Loader } from 'components';

type Props = {
  displayedList: any[];
  loadingList: boolean;
  loadingSearchedList: boolean;
  detailsPath: string;
};

const ListLayout = ({ displayedList, loadingList, loadingSearchedList, detailsPath }: Props) => {
  const { onSearch, query } = useRootContext();

  return (
    <>
      <Row className="justify-content-center flex-wrap">
        <Col className="mb-5 px-5">
          <SearchFilter placeholder="Search..." searchTerm={query} onSearch={onSearch} />
        </Col>
        {(!loadingList || !loadingSearchedList) && displayedList ? (
          <>
            {displayedList.length > 0 ? (
              displayedList.map((x: any) => (
                <Col key={x.id} className="col-sm-6 px-5">
                  <CardItem item={x} detailsPath={detailsPath} />
                </Col>
              ))
            ) : (
              <div>No results</div>
            )}
          </>
        ) : (
          <Loader />
        )}
      </Row>
    </>
  );
};

export default ListLayout;
