import styled from 'styled-components';

const Input = styled.input`
  width: 100%;
  padding: 0.5rem 1rem;
  margin: 0.5rem 0;
  border-radius: 4px;
  background-color: var(--secondary);
  color: var(--light);
  border: 0;
`;

export default Input;
