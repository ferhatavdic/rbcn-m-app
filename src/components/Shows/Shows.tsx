import React from 'react';

import { API_PATHS } from 'utils/constants';
import useApi from 'utils/hooks/useApi';
import useToggleList from 'utils/hooks/useToggleList';
import { useRootContext } from 'context/RootContext';
import { ListLayout } from 'components';

const Shows = () => {
  const { resultSearchShows, loadingSearchShows } = useRootContext();
  const [{ result: resultShows, loading: loadingShows }] = useApi(API_PATHS.SHOWS_TOP);
  const shows = useToggleList(resultShows, loadingShows, resultSearchShows, loadingSearchShows);

  return (
    <ListLayout
      displayedList={shows}
      loadingList={loadingShows}
      loadingSearchedList={loadingSearchShows}
      detailsPath={'shows'}
    />
  );
};

export default Shows;
