import { createGlobalStyle } from 'styled-components';
import { BREAKPOINTS } from 'utils/constants';

export default createGlobalStyle`
:root{
  --background: #222831;
  --ternary: #184d47;
  --secondary: #393e46;
  --primary: #d65a31;
  --light: #eeeeee;
}
  * {
    font-family: Calibri, 'Trebuchet MS', sans-serif;
    box-sizing: border-box;
    font-size: 18px;
  }

  html,
  body {
    margin: 0;
    height: 100%;
    scroll-behavior: smooth;
    /* background: #212028; */
    background-color: var(--background);
    color: var(--light);
  }

  #root {
    display: flex;
    flex-direction: column;
    align-items: center;
    height: 100%;
    width: 100%;
  }
  main {
    flex: 1 0 auto;
    width: 100%;
  }


  button {
    border: 0;
    padding: 0;
    cursor: pointer;
    outline: none;

    &:focus {
      outline: none;
    }
  }

  a {
    text-decoration: none;

    &:hover {
      text-decoration: none;
    }
  }

  h1 {
    font-size: 4rem;
  }
  h2 {
    font-size: 3rem;
    margin: 1rem 0;
  }
  h3 {
    font-size: 2.5rem;
  }
  h4 {
    font-size: 2rem;
  }
  h5 {
    font-size: 1.5rem;
  }
  h6 {
    font-size: 1rem;
  }

  .clickable {
    cursor: pointer;
  }
  .text-center {
    text-align: center;
  }
  .justify-content-center {
    justify-content: center;
  }
  .flex-wrap {
    flex-wrap: wrap;
  }

  .mb-5 {
    margin-bottom: 2rem;
  }
  .mb-3{
    margin-bottom: 1rem;
  }
  .mt-3{
    margin-top: 1rem;
  }
  .px-5 {
    padding-left: 2rem;
    padding-right: 2rem;

  }
  .d-flex{
    display: flex;
  }
  .justify-space-between{
    justify-content: space-between;
  }
  .align-items-center{
    align-items: center;
  }
  .mt-0{
    margin-top:0;
  }
  .mr-5{
    margin-right: 2rem;
  }
  iframe{
    border:0;
  }
  .video-wrapper {
  position: relative;
  padding-bottom: 56.25%; /* 16:9 */
  height: 0;
}
.video-wrapper iframe {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
}
.h-100{
  height: 100%;
}
.mr-2{
  margin-right: 1rem;
}
.text-justify{
  text-align:justify;
}
/* . */

@media only screen and (min-width: ${BREAKPOINTS.SM}px) {
    .col-sm-6 {
      flex: 0 0 50%;
      max-width: 50%;
    }
  }

  @media only screen and (max-width: ${BREAKPOINTS.SM}px) {
    .px-5{
      padding-left: 1rem;
    padding-right: 1rem;
    }
    h2{
      font-size: 2rem;
    }
  }

`;
