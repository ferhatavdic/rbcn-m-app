import React, { useMemo, useContext, useEffect, useState, useCallback } from 'react';
import { API_PATHS, MIN_CHARS_SEARCH } from 'utils/constants';
import useApi from 'utils/hooks/useApi';
import { useLocation } from 'react-router-dom';
import routes from 'utils/routes';

type Props = {
  children: React.ReactNode;
};

type ProviderProps = {
  onSearch?: (value: string) => void;
  resultSearchMovies?: any;
  loadingSearchMovies?: boolean;
  errorSearchMovies?: any;
  resultSearchShows?: any;
  loadingSearchShows?: boolean;
  errorSearchShows?: any;
  query?: string;
  currentPath: string;
};

const RootContext = React.createContext<ProviderProps | undefined>(undefined);

export const RootProvider = ({ children }: Props) => {
  const location: any = useLocation();
  const [currentPath, setCurrentPath] = useState<string>(location.pathname);
  const [query, setQuery] = useState<string>('');

  const [
    { result: resultSearchMovies, loading: loadingSearchMovies, error: errorSearchMovies },
    searchMovies,
  ] = useApi(API_PATHS.MOVIES_SEARCH(query), { initialFetch: false });

  const [
    { result: resultSearchShows, loading: loadingSearchShows, error: errorSearchShows },
    searchShows,
  ] = useApi(API_PATHS.SHOWS_SEARCH(query), { initialFetch: false });

  // prevent unnecessary api calls when react detects location object ref change, but location.pathname is same
  useEffect(() => {
    setCurrentPath(location.pathname);
  }, [location, setCurrentPath]);

  useEffect(() => {
    if (query && query.length >= MIN_CHARS_SEARCH) {
      if (currentPath === routes.SHOWS.path || currentPath === routes.HOME.path) searchShows();
      if (currentPath === routes.MOVIES.path) searchMovies();
    }
  }, [query, searchShows, searchMovies, currentPath]);

  const onSearch = useCallback(
    (value: string) => {
      setQuery(value);
    },
    [setQuery]
  );

  const providerValue: ProviderProps = useMemo(
    () => ({
      onSearch,
      resultSearchMovies,
      loadingSearchMovies,
      errorSearchMovies,
      resultSearchShows,
      loadingSearchShows,
      errorSearchShows,
      query,
      currentPath,
    }),
    [
      onSearch,
      resultSearchMovies,
      loadingSearchMovies,
      errorSearchMovies,
      resultSearchShows,
      loadingSearchShows,
      errorSearchShows,
      query,
      currentPath,
    ]
  );

  return <RootContext.Provider value={providerValue}>{children}</RootContext.Provider>;
};

export function useRootContext() {
  const context: ProviderProps = useContext(RootContext);
  if (!context) {
    throw new Error(`useRootContext must be used within a RootProvider`);
  }
  return context;
}

export const Consumer = RootContext.Consumer;
